# Raspberry Pi 5 DSI screen cable

This is a design for a DSI cable specific for the Raspberry Pi 5. The idea is
to be able to use the old DSI screens already available for Raspberry Pi 1-4,
but without the problems of some of the commercially available cables.

![The new design, in a 5" screen](cable-final.jpg)

## Why?

I recently bought a RPi5 and a RPi 5" screen, but found that the DSI connector
in the Raspberry has changed, so the flat cable that comes with the screen is
useless. After buying a new cable, I found that it requires either to twist it,
or to put the RPi5 upside down.

![The commercial cable, connected with the RPi5 in the normal position](cable-original-1.jpg)

![The commercial cable, connected with the RPi5 upside-down](cable-original-2.jpg)

Also, putting the RPi5 backwards wasn't a solution, because the board covered the
screen connector.

![The RPi5 placed backwards](del-reves.jpg)

For this reason, I designed and built my own cable.

## Design

The cable is designed with Kicad. Care has been taken to ensure that the length
of the three differential pairs (Clock, Data1 and Data2) are as close as possible,
to avoid clock skew problems.

![The board schematics in Kicad](schematic_capture.png)

![The board design in Kicad](kicad-capture.jpg)

If you send it to manufacture, remember to specify that you want *stiffeners* in
the connectors.

## About

Sergio Costas Rodríguez  
Raster Software Vigo  
https://www.rastersoft.com  
rastersoft@gmail.com
